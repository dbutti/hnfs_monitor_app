# hnfs_monitor_app

App to control a Basler camera to acquire images during a HNFS experiment. 

## Installation notes

At this stage, the application is launched as a standard Python script. First, from the Basler website download and install the Pylon software, needed to control the camera. Then, some Python packages have to be manually installed with `pip install pyqtgraph scipy pypylon`. To avoid compatibility issues, it is recommended to work on a (dedicated) Python virtual environment. Once the environment is ready, clone locally the folder </hnfs_monitor_app> of this repo, move there and type `python hnfs_monitor_app.py` to launch the application.

NB: using Anaconda, the Navigator tool facilitates the setup of the Python virtual environment. Nevertheless, the pypylon package must be installed manually from the conda terminal with pip as it is not listed in the standard conda repository.

## Widgets and usage

The application consists of a display panel (left) and a control panel (right). 
The display panel has two tabs: an **Image** to monitor the stream of images from the camera and a **Power Spectrum** to compute, from time to time, the 2D power spectrum of the current image.

### Camera settings

Connecting a camera is quite straightforward. From the **Camera** combobox, select one of the available cameras. The combobox is populated with all the detected cameras plus a TEST one, discussed below. If the camera is opened with success, the fields **Exposure** and **Gain** are updated with the corresponding parameters read from the device. The GUI tries also to set the highest **Pixel Format**, then displayed in the respective label.

Images can be acquired in two modes: the **Shot** button takes the last frame only while **Live** displays the stream of images coming from the camera, updated every **Rfr time** interval (see also *Known issues*).

#### Reference subtraction

The application allows to subtract a reference image to the current frame, in order to diesentangle the speckle random features from the almost constant background pattern. Two strategied are implemented for reference substraction. The **Grab** button take the current frame and store it as reference. Otherwise, if the **Grab last** is toggled, the current frame is stored as reference for the next image and the reference is always renewed as the new frame is available. This is useful to compensate for a non-static background that varies with characteristic time slower than the frame rate. Irrespective of the grab strategy, the reference substraction is triggered by toggling the *Subtract* button.

#### Save frames

The raw (i.e. without reference subtraction or ROI) frames can be directly saved from the application. First, a saving path is typed in **Save path**. The application checks if the folder exists and, if not, it creates the asked folder or folder tree. An error is raised if this process fails (e.g. permission denied). To save a sequence of pictures, enter the desired number of frames in the line edit **Save seq.** and toggle **Autosave**. The button turns red and each new frame is saved until the requested sequence length is reached. Frames are saved irrespective of their acquisition mode, Shot or Live. When the sequence is over, the autosave button turns green and the sequence length reset to an arbitrary value of 50.

### Image tab

The main widget of the image panel is the display streaming the camera frames. This is a standard pyqtgraph PlotWidget allowing basic interaction features (zoom/pan). Refer to the corresponding pyqtgraph documentation for a complete description. A square region of interest (**ROI**) of an arbitrary **ROI size** can be drawn to restrict the analysis on a portion of the frame. At the bottom, one can find the average intensity (**Avg.**) of the current image or, if the ROI is active, of the ROI itself. This is useful to keep under control the level of illumination at the camera. The time evolution of the average can be plotted in another pyqtgraph PlotWidget below the main image by toggling the **Trace On** button. To clear the trace canvas, press **Trace Clear**.

<img src="screenshot_img.PNG" width="600">

From time to time it may be useful to extract the power spectrum of the current frame, to see whether a speckle signal is present or not. To do so, press **Get PS** in the Power spectrum settings frame. Note that the PS computation requires a square image so a ROI must be active. Since the PS computation requires a FFT operation, the stream from the camera is automatically stopped to avoid an overload of the GUI. The fields **Magnification** and **Pixel size** are not strictly required to compute the PS matrix but are needed if one wants the correct scaling on the wavevector axes. Once the PS is available, the GUI automatically switched to the Power Spectrum tab.

### Power Spectrum tab

The appearence closely resembles the image tab. On top, we find a PlotWidget displaying the computed PS. At the bottom, some radial profiles can be extracted for a more quantitative analysis. To get a profile, edit the **Ang. centre** and **Ang. width** with the position and extension of the angular sector you want to analyse and then click **Get profile** to plot the curve on the corresponding widget. Several curves can be superimposed on the same plot. Press **Clear profile** when you want to clear the profile canvas. The coordinates for the angles follow the usual cartesian convention: 0 deg represents the positiove x-axis and the polar angle ranges between -180 and +180 deg.

NB: both the PS 2D image and the profile intensities are plotted in logaritmic scale.

<img src="screenshot_ps.PNG" width="600">

## Test mode

If a physical camera is not available, it is still possible to use the application in **TEST** mode. To do so, you need a directory with some sample images that will be used a dummy camera frames. The path to this folder is typed in **Test folder**. Selecting the TEST item from the combo box, the application starts cycling over the sample images and, except for the device controls (exposure and gain), the interaction with the application is the same as the case of a physical camera.  

## Errors

While using the GUI, keep always under control the prompt from which the app was launched. Message errors are raised there as well as feedbacks about the outcome of some operations (e.g. open/close camera, autosave).

## Known issues

Basler cameras support frame rates up to ~50 Hz. This PyQt GUI cannot stand such speed, especially in case of high-resolution frames or when some image operation is active (e.g. average or background subtraction). To prevent the application from crashing, please do keep the **Rfr. time** at a sufficient value. On a standard laptop, 1000 ms (1 Hz) should allow a rather smooth operation. This value can be decreased with a more powerful computer.
