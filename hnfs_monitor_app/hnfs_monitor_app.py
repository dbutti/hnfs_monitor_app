#!/usr/bin/env python3

import os
import sys

import numpy as np
from datetime import datetime
from PyQt5 import QtWidgets, QtCore, uic

import my_camera
from analyser import Analyser
from image_utils import Image_utils

class Ui(QtWidgets.QMainWindow):
    
    signal_stop_live_camera = QtCore.pyqtSignal()
    
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi("hnfs_monitor_app.ui", self)
             
        self.init_widgets()
        
        self.connect_widgets()
        
        self.init_available_cameras()
        
        self.analyser = Analyser()
        
        self.show()
  
    def init_widgets(self):
        
        self.default_frames_to_save = 50
        folder_test = "Type path to test images"
        folder_save = "Type path to autosave folder"
        self.lineEdit_test_folder.setText(folder_test)
        self.lineEdit_save_folder.setText(folder_save)
        self.lineEdit_frames_to_save.setText(\
                                        str(self.default_frames_to_save))
        self.btn_autosave.setStyleSheet("background-color: green")

        self.img_panel.img_widget.set_xlabel(x_label="x [px]")
        self.img_panel.img_widget.set_ylabel(y_label="y [px]")
        self.trace_widget.set_xlabel(x_label="frame #")
        self.trace_widget.set_ylabel(y_label="Intensity [arb.u.]")
        self.prof_widget.set_xlabel(x_label="q [1/um]")
        self.prof_widget.set_ylabel(y_label="Intensity [arb.u.]")
        self.ps_panel.ps_widget.set_xlabel(x_label="qx [1/um]")
        self.ps_panel.ps_widget.set_ylabel(y_label="qy [1/um]")
        self.prof_widget.set_semilog()
        
        
    def connect_widgets(self):

        self.comboBox_cameras.activated.connect(self.on_camera_changed)
        self.lineEdit_exposure.editingFinished.connect(
                                            self.on_exposure_changed)
        self.lineEdit_gain.editingFinished.connect(
                                            self.on_gain_changed)
        self.lineEdit_refresh_time.editingFinished.connect(
                                            self.on_refresh_time_changed)
        self.btn_live.clicked.connect(self.on_live_clicked)
        self.btn_shot.clicked.connect(self.on_shot_clicked)
        self.btn_grab_ref_img.clicked.connect(self.on_grab_ref_img_clicked)
        self.btn_last_ref_img.clicked.connect(self.on_last_ref_img_clicked)
        self.img_panel.ROI.sigRegionChanged.connect(self.on_ROI_img_updated)
        self.btn_autosave.clicked.connect(self.on_autosave_clicked)
        self.btn_trace_on.clicked.connect(self.on_trace_on_clicked)
        self.btn_trace_clear.clicked.connect(self.on_trace_clear_clicked)
        self.lineEdit_magnification.editingFinished.connect(
                                            self.on_update_optics)
        self.lineEdit_px_size.editingFinished.connect(
                                            self.on_update_optics)
        self.btn_get_ps.clicked.connect(self.on_get_ps_clicked)
        self.btn_get_prof.clicked.connect(self.on_get_prof_clicked)
        self.btn_clear_prof.clicked.connect(self.on_clear_prof_clicked)

    def init_available_cameras(self):
        
        self.camera_names = my_camera.get_available_camera_names()
        
        self.camera_labels = ["Select"]
        i = 0
        for cam_name in self.camera_names:
            if cam_name != "":
                cam_label = cam_name
            else:
                cam_label = f"unnamed cam {i:d}"
                i += 1
            self.camera_labels.append(cam_label)
            
        self.comboBox_cameras.addItems(self.camera_labels)
        
        self.current_camera_label = "Select"
        self.camera_is_open = False
        
            
    def open_camera(self, camera_name):
        self.camera_thread = QtCore.QThread()
        self.camera = my_camera.QCamera(camera_name)
        
        if camera_name == "TEST":
            self.camera.basler_cam.set_test_folder(\
                                            self.lineEdit_test_folder.text())
        
        if not self.camera.basler_cam.is_open():
            self.camera_is_open = False
        
        else:
            self.signal_stop_live_camera.connect(self.camera.stop_live)
            self.camera.moveToThread(self.camera_thread)
            self.camera_thread.started.connect(self.camera.start_live)
            self.camera.signal_new_frame.connect(self.on_new_frame)
            
            self.camera.basler_cam.set_pixel_format("Mono16")
            
            self.lineEdit_exposure.setText(
                str(self.camera.basler_cam.get_exposure()))
            self.lineEdit_gain.setText(
                str(self.camera.basler_cam.get_gain()))
            self.lineEdit_refresh_time.setText(
                str(int(self.camera.refresh_time * 1000)))
            self.label_px_format.setText(str(
                                    self.camera.basler_cam.get_pixel_format()))
                       
            self.camera_is_open = True

    def get_save_folder(self):
        
        folder_path = self.lineEdit_save_folder.text()
        path_exists = os.path.isdir(folder_path)
        
        if not path_exists:
            
            try:
                os.makedirs(folder_path)
                print(f"Created new directory: {folder_path}")
                
                return os.path.join(folder_path, "") # add final slash
                
            except Exception as e:
                print(e)
                print("Cannot create directory! Check path/permission")
                
                return None    

        else:
            
            return os.path.join(folder_path, "") # add final slash


    def save_frame(self, folder_path):

        time_str = datetime.now().strftime("%d-%m-%Y_%H-%M-%S_%f")[:-4]
        full_path = f"{folder_path}{time_str}.tiff"
        
        try:
            Image_utils.save_image(full_path, self.data)
            print(f"Saved {full_path}")
            return True
            
        except Exception as e:
            print(e)
            print("Cannot save frame!")
            return False

    def autosave(self):
        
        folder_path = self.get_save_folder()
                
        if folder_path is None:
            self.lineEdit_frames_to_save.setText(\
                                            str(self.default_frames_to_save))
            self.btn_autosave.setChecked(False)
            self.btn_autosave.setStyleSheet("background-color: green")
            
        else:
            frames_to_save = int(self.lineEdit_frames_to_save.text())
        
            if frames_to_save == 0:
                self.lineEdit_frames_to_save.setText(\
                                            str(self.default_frames_to_save))
                self.btn_autosave.setChecked(False)
                self.btn_autosave.setStyleSheet("background-color: green")
            
            elif frames_to_save > 0:
                self.btn_autosave.setStyleSheet("background-color: red")
                save_success = self.save_frame(folder_path)
                if save_success:
                    self.lineEdit_frames_to_save.setText(str(frames_to_save-1))
                    
    def on_new_frame(self):
        
        if self.btn_last_ref_img.isChecked():   # data_ref = previous frame
            self.data_ref = self.frame

        self.frame = self.camera.frame
        
        if self.btn_subtract_ref_img.isChecked():        
            self.data = self.frame - self.data_ref
        else:
            self.data = self.frame
                        
        self.img_panel.img_widget.imshow(self.data)
        
        self.on_ROI_img_updated()
        
        if self.btn_autosave.isChecked():
            self.autosave()

    def on_camera_changed(self):
        
        new_camera_label = self.comboBox_cameras.currentText()
        
        if new_camera_label == self.current_camera_label:
            return
        
        else:
            if self.camera_is_open:
                self.camera.close()
                self.camera_is_open = False
                print(f"Closed camera {self.current_camera_label}")
            
            if new_camera_label != "Select":     
                new_camera_idx = self.camera_labels.index(new_camera_label)-1
                new_camera_name = self.camera_names[new_camera_idx]
                
                self.open_camera(new_camera_name)
                
                if self.camera_is_open:
                    print(f"Opened camera {new_camera_label}")
                else:
                    self.comboBox_cameras.setCurrentIndex(0)
                    print(f"Failed to open camera {new_camera_label}")
                
            self.current_camera_label = self.comboBox_cameras.currentText()

    def on_autosave_clicked(self):
        if self.btn_autosave.isChecked():
            self.btn_autosave.setStyleSheet("background-color: red")    
        else:
            self.btn_autosave.setStyleSheet("background-color: green")    

    def on_trace_on_clicked(self):
        if self.btn_trace_on.isChecked():
            self.trace_widget.setEnabled(True)
            self.trace_curve = self.trace_widget.plot(pen="r")
            self.trace_data = []
            self.on_ROI_img_updated()
            
        else:
            self.trace_widget.clear()
            self.trace_widget.setEnabled(False)
            

    def on_trace_clear_clicked(self):
        self.trace_data = []

    def on_ROI_img_updated(self):
  
        try:
            if self.img_panel.btn_ROI.isChecked():       
                masked_data, coord = self.img_panel.ROI.getArrayRegion(\
                                            self.data.T, 
                                            self.img_panel.img_widget.img_item,
                                            returnMappedCoords=True)
                self.masked_data = masked_data.T
                
                # self.std = int(np.std(self.masked_data))
                self.avg = int(np.nanmean(self.masked_data))

            else:
                # self.std = int(np.std(self.data))
                self.avg = int(np.nanmean(self.data))
                
            # self.label_std.setText(str(self.std))
            self.label_avg.setText(str(self.avg))
            
            if self.btn_trace_on.isChecked():
                self.trace_data.append(self.avg)
                self.trace_curve.setData(self.trace_data)
           
                if len(self.trace_data) > 1000:
                    self.trace_data.pop(0)
                    
        except Exception as e:
            print(e)
            print("Failed to get average!")
            pass
              
    def on_shot_clicked(self):
        self.camera.shot()
        
    def on_live_clicked(self):
        if self.btn_live.isChecked():
            self.camera_thread.start()
            self.comboBox_cameras.setEnabled(False)
        else:
            self.signal_stop_live_camera.emit()
            self.camera_thread.quit()
            self.comboBox_cameras.setEnabled(True)
        
    def on_grab_ref_img_clicked(self):
        self.data_ref = self.data
        self.btn_subtract_ref_img.setEnabled(True)
        
    def on_last_ref_img_clicked(self):
        self.btn_subtract_ref_img.setEnabled(True)

    def on_subtract_ref_img_clicked(self):
        # just refresh the img
        self.on_new_frame()

    def on_exposure_changed(self):
        exposure_us = int(self.lineEdit_exposure.text())
        self.camera.basler_cam.set_exposure(exposure_us)
        exposure_us_read = self.camera.basler_cam.get_exposure()
        print(f"Current exposure {exposure_us_read} us")
 
    def on_gain_changed(self):
        gain = int(self.lineEdit_gain.text())
        self.camera.basler_cam.set_gain(gain)
        gain_read = self.camera.basler_cam.get_gain()
        print(f"Current gain {gain_read}")
 
    def on_refresh_time_changed(self):
        refresh_time_ms = float(self.lineEdit_refresh_time.text())
        self.camera.set_refresh_time(refresh_time_ms)

    def on_update_optics(self):
        M = float(self.lineEdit_magnification.text())
        pixel_size = float(self.lineEdit_px_size.text()) * 1e-6
        self.analyser.update_optics(M, pixel_size)

    def on_px_size_changed(self):
        self.analyser.magnification = float(self.lineEdit_magnification.text())

    def on_get_ps_clicked(self):
        # stop any live operation
        self.btn_live.setChecked(False)
        self.btn_trace_on.setChecked(False)
        self.on_live_clicked()
        self.on_trace_on_clicked()

        if self.img_panel.btn_ROI.isChecked():       
            data = self.masked_data
        else:
            data = self.data
        
        self.ps_dict = self.analyser.compute_ps(data)
        
        extent = [self.ps_dict["xax"][0], self.ps_dict["xax"][-1],
                  self.ps_dict["yax"][0], self.ps_dict["yax"][-1]]
        extent = np.array(extent) * 1e-6
        
        self.ps_panel.ps_widget.imshow(np.log10(self.ps_dict["data"]), 
                                        extent=extent)
        
        self.tab_widget.setCurrentIndex(1)
        
    def on_get_prof_clicked(self):
        ang_centre = float(self.lineEdit_ang_centre.text())
        ang_width = float(self.lineEdit_ang_width.text())
        
        prof_dict = self.analyser.sector_average(self.ps_dict, \
                                                 ang_centre, ang_width)
        
        curve = self.prof_widget.plot_curve(prof_dict["ax"] * 1e-6,
                                            prof_dict["data"], 
                                            pen="b")

    def on_clear_prof_clicked(self):
        self.prof_widget.clear_all()
        
    def find_idx(self, value, array):
        return np.argmin(np.abs(value - array))
    
    def prtst(self):
        print("Test func")
        
app = QtWidgets.QApplication(sys.argv)
window = Ui()
app.exec_()