import os

import numpy as np
import time
from PyQt5.QtCore import QObject, pyqtSignal

from basler_wrapper import BaslerCam, get_available_cameras
from image_utils import Image_utils

class BaslerCam_test():
    """ Class to provide same interaction as a dummy BaslerCam """
    def __init__(self, camera_name):
        self.is_open_flag = False
        self.is_grabbing_flag = False
        self.exposure = 0
        self.gain = 1
        self.pixel_format = "None"

    def set_test_folder(self, test_folder):
        self.test_folder = test_folder
    
    def is_open(self):
             
        try:        
            files = os.listdir(self.test_folder)
            
            files = files[:15]  #to reduce opening time
            
            self.datas = []
            for file in files:
                data = Image_utils.get_image_data(
                    os.path.join(self.test_folder, file), transpose_flag=False)
                
                self.datas.append(data)
            
            self.is_open_flag = True
            self.frame_idx = 0
        
        except Exception as e:
            print(e)
            print("Could not open TEST folder!")
            self.is_open_flag = False
            
        return self.is_open_flag
        
    def close(self):
        self.is_open_flag = False
    
    def start(self, number_of_images):
        self.is_grabbing_flag = True
        
    def stop(self):
        self.is_grabbing_flag = False

    def get_next_image(self, time_out):
        
        timestamp = time.time()
        data = self.datas[self.frame_idx]
        
        if self.frame_idx == len(self.datas) - 1:
            self.frame_idx = 0
        else:
            self.frame_idx += 1
        
        return timestamp, data

    def get_exposure(self):
        return self.exposure
    
    def set_exposure(self, exposure_us):
        self.exposure = exposure_us

    def get_gain(self):
        return self.gain
    
    def set_gain(self, gain):
        self.gain = gain

    def get_pixel_format(self):
        return self.pixel_format

    def set_pixel_format(self, pixel_format):
        self.pixel_format = pixel_format

def get_available_camera_names():
    available_cameras = get_available_cameras()
    
    if len(available_cameras) >= 1:
        available_camera_names = [cam[0] for cam in available_cameras]
    else:
	available_camera_names = []
        print("No physical camera available!")
    
    available_camera_names.append("TEST")
    
    return available_camera_names

class QCamera(QObject):

    signal_new_frame = pyqtSignal()
    
    def __init__(self, camera_name):
        super().__init__()
        
        self.refresh_time = 1
        
        if camera_name=="TEST":
            self.basler_cam = BaslerCam_test(camera_name)
        else:
            self.basler_cam = BaslerCam(camera_name)
        
    def close(self):
        self.basler_cam.close()
        
    def start_live(self):
        self.is_live = True
        self.basler_cam.start(number_of_images=-1)
        
        while self.is_live:
            self.get_frame()
            time.sleep(self.refresh_time)
    
        self.basler_cam.stop()
    
    def stop_live(self):
        self.is_live = False
    
    def shot(self):     
        self.basler_cam.start(number_of_images=1)
        self.get_frame()
        self.basler_cam.stop()
        
    def get_frame(self):
        try:
            timestamp, data = self.basler_cam.get_next_image(time_out=1000)
        except:
            data = None
            
        if data is not None:
            frame = np.array(data)
            self.frame = frame
            self.signal_new_frame.emit()
            
    def set_refresh_time(self, refresh_time_ms):
        self.refresh_time = refresh_time_ms / 1000
