import edf_file
import numpy as np
import pickle as pk
from PIL import Image


class Image_utils:
    allowed_extensions = ['tif', 'tiff', 'edf', 'pkGT']
    
    @classmethod    
    def read_edf_image(cls, image_full_Path):
        return np.array(edf_file.EdfFile(\
                                image_full_Path).GetData(0)).astype(np.float)

    @classmethod    
    def read_pkGT_image(cls, image_full_Path):
        return pk.load(open(image_full_Path, "rb"))['data']
    
    @classmethod
    def read_generic_image(cls, image_full_Path):        
        return np.array(Image.open(image_full_Path))
        
    @classmethod    
    def get_image_data(cls, image_full_Path, transpose_flag=False):
        extension = image_full_Path.split('.')[-1]
        if extension in cls.allowed_extensions:
            if extension == 'edf':
                image = cls.read_edf_image(image_full_Path)    
            elif extension == 'pkGT':
                image = cls.read_pkGT_image(image_full_Path)
            else:
                image = cls.read_generic_image(image_full_Path)
            return np.transpose(image) if transpose_flag else image 
        else:
            return None
        
    @classmethod
    def save_image(cls, image_full_Path, data):
        # Image.fromarray(data).save(f"{image_full_Path}")
        Image.fromarray(data, mode="I;16").save(f"{image_full_Path}")
