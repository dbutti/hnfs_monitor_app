import numpy as np
import pyqtgraph as pg
from PyQt5 import QtWidgets, uic

pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

lut = pg.ColorMap(
                np.linspace(0, 1, 6),
                np.array([(0, 0, 128), (0, 77, 255), (41, 255, 205),
                        (205, 255, 41), (255, 104, 0), (128, 0, 0)]
                         )).getLookupTable(alpha=False)
      
class ImgPanel(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("img_panel.ui", self)
                
        self.ROI_size = int(self.lineEdit_ROI_size.text())
        self.init_ROI()

        self.connect_widgets()
        
        self.show()
               
    def connect_widgets(self):
        self.lineEdit_ROI_size.editingFinished.connect(\
                                        self.on_ROI_size_changed)
        self.btn_ROI.clicked.connect(self.on_click_btn_ROI)
        
        self.prox_mouse_hover = pg.SignalProxy(\
                                        self.img_widget.scene().sigMouseMoved,
                                        rateLimit=30, slot=self.on_mouse_moved)

    def init_ROI(self):
              
        self.ROI = pg.ROI(pos=[0, 0],
                          size=[self.ROI_size, self.ROI_size],
                          angle=0,
                          pen=pg.mkPen("r"))
        
        self.img_widget.addItem(self.ROI)
        self.ROI.setZValue(20)
        self.ROI.hide()

    def on_click_btn_ROI(self):
        
        if self.btn_ROI.isChecked():
            self.ROI.show()
        else:
            self.ROI.hide()
            
    def on_ROI_size_changed(self):
        self.ROI_size = int(self.lineEdit_ROI_size.text())
        self.ROI.setSize(self.ROI_size)

    def on_mouse_moved(self, event):
        pos_scene = event[0]  
        if self.img_widget.sceneBoundingRect().contains(pos_scene):
            pos_coord = self.img_widget.plotItem.vb.mapSceneToView(pos_scene)
            self.label_coord.setText(\
                        f"(x, y) = ({pos_coord.x():.2f}, {pos_coord.y():.2f})")
        else:
            self.label_coord.setText("(x, y) = ( , )")

class PsPanel(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("ps_panel.ui", self)
                
        self.connect_widgets()
               
        self.show()
                
    def connect_widgets(self):
        self.prox_mouse_hover = pg.SignalProxy(\
                                        self.ps_widget.scene().sigMouseMoved,
                                        rateLimit=30, slot=self.on_mouse_moved)       
          
    def on_mouse_moved(self, event):
        pos_scene = event[0]  
        if self.ps_widget.sceneBoundingRect().contains(pos_scene):
            pos_coord = self.ps_widget.plotItem.vb.mapSceneToView(pos_scene)
            self.label_coord.setText(\
                        f"(x, y) = ({pos_coord.x():.2f}, {pos_coord.y():.2f})")
        else:
            self.label_coord.setText("(x, y) = ( , )")
            
class MyPlotWidget(pg.PlotWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        
        self.colors = ["b", "g", "r", "c", "m", "y", "k"]
        self.current_color = 0
        
    def plot_curve(self, x, y, **kwargs):
        x_finite = x[np.isfinite(y)]
        y_finite = y[np.isfinite(y)]
        
        color = kwargs.get("color", None)
        linewidth = kwargs.get("linewidth", 1)
        
        if color is None:
            color = self.colors[self.current_color]
            if self.current_color < len(self.colors) - 1:
                self.current_color += 1
            else:
                self.current_color = 0

        pen = pg.mkPen(color=color, width=linewidth)
        curve = self.plot(pen=pen)
        curve.setData(x_finite, y_finite)
        return curve
   
    def update_curve(self, curve, x, y):
        curve.setData(x, y)
        
    def set_xlabel(self, x_label):
        self.setLabel("bottom", text=x_label)
        
    def set_ylabel(self, y_label):
        self.setLabel("left", text=y_label)
        
    def set_semilog(self):
        self.setLogMode(False, True)
        
    def clear_all(self):
        self.clear()
        self.current_color = 0
        
           
class MyImageWidget(pg.PlotWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        
        self.img_item = pg.ImageItem()
        self.addItem(self.img_item)
        
    def imshow(self, data, extent=None):
        
        self.img_item.setImage(data.T)
        
        if extent is not None:
            x_0 = extent[0]
            y_0 = extent[2]
            w = extent[1] - extent[0]
            h = extent[3] - extent[2]
            rect = pg.Qt.QtCore.QRectF(x_0, y_0, w, h)
            
            self.img_item.setRect(rect)

    def set_xlabel(self, x_label):
        self.setLabel("bottom", text=x_label)
        
    def set_ylabel(self, y_label):
        self.setLabel("left", text=y_label)
        