"""

Module to interact with Basler cameras that wraps the most useful methods of 
pypylon.

Developed by E. Bravin (CERN SY-BI-PM)

Documentation: 
    https://be-bi-pm-eos.web.cern.ch/docs/basler_cam_docs_0.3.1/index.html

"""

import sys
import numpy as np
from traceback import print_exc
from typing import Optional, Any

from pypylon import pylon
from pypylon import genicam

ACQUISITION_FIELDS = ["Gain", "ExposureTime", "Gamma", "BlackLevel",
                      "BinningHorizontal", "BinningVertical",
                      "OffsetX", "OffsetY", "Width", "Height", "PixelFormat"]

SETTING_FIELDS = ["Gain", "GainAuto", "BlackLevel", "GammaEnable",
                  "GammaSelector", "Gamma", "DigitalShift",
                  "ReverseX", "ReverseY", "BinningHorizontalMode",
                  "BinningVerticalMode", "ExposureAuto",
                  "ExposureTime", "AcquisitionFrameRate",
                  "AcquisitionFrameRateEnable",
                  "BinningHorizontal", "BinningVertical", "OffsetX", "OffsetY",
                  "Width", "Height",
                  "GevSCPD", "GevSCPSPacketSize", "PixelFormat",
                  "TriggerDelay", "TriggerActivation", "TriggerMode",
                  "TriggerSelector", "TriggerSource", "TestImageSelector"]

PixelFormats = ["Mono8", "Mono12", "Mono16"]
"""Frequently used pixel formats."""

TriggerModes = ["Off", "AcquisitionStart", "FrameStart", "LineStart"]
"""Trigger modes"""

TriggerSources = ["Software", "Line1", "Line2", "Line3", "Line4", "Line5",
                  "Line6", "Line6", "Line7", "Line8", "CC1", "CC2", "CC3",
                  "CC4"]
"""Trigger selectors."""

TriggerEdges = ["RisingEdge", "FallingEdge", "AnyEdge", "LevelHigh",
                "LevelLow"]
"""Trigger edge."""

TestImages = ["Off", "Testimage1", "Testimage2", "Testimage3", "Testimage4",
              "Testimage5", "Testimage6", "Testimage7"]
"""Test images."""

LOG_INFO = 0
LOG_WARNING = 1
LOG_ERROR = 2


def get_available_cameras() -> list:
    """Returns the list of discoverable cameras.

    Returns:
        A list of lists. each list contains:

        - The user defined name of the camera
        - The class of the camera
        - The model name
        - The serial number:

    """

    cameras = []
    factory = pylon.TlFactory.GetInstance()
    available_cameras = factory.EnumerateDevices()
    for cam in available_cameras:
        cls = cam.GetDeviceClass()
        if cls == "BaslerUsb":
            name = cam.GetFriendlyName()
        else:
            name = cam.GetUserDefinedName()

        cameras.append([name, cam.GetDeviceClass(), cam.GetModelName(),
                        cam.GetSerialNumber()])
    return cameras

class ImageData(np.ndarray):
    """Numpy Array with extra fields: *t_stamp* and *pixel_format*."""

    def __new__(cls, array, dtype=None, order=None, t_stamp=0, pixel_format=0):
        obj = np.asarray(array, dtype=dtype, order=order).view(cls)
        obj.t_stamp = t_stamp
        obj.pixel_format = pixel_format
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        self.t_stamp = getattr(obj, 't_stamp', None)
        self.pixel_format = getattr(obj, 'pixel_format', None)

    def like(self, other):
        """If *self* and *other* have the same shape and pixel format
        returns True, returns False otherwise"""

        try:
            if (np.array_equal(self.shape, other.shape) and
                self.pixel_format == other.pixel_format):
                return True
        except:
            pass

        return False

    def clone(self):
        return ImageData(self, t_stamp=self.t_stamp, pixel_format=self.pixel_format)

class BaslerCam(object):
    """Class that wraps the most common pypylon methods.

    BASLER cameras operates on the grabbing cycle concept. the cycle is started
    by *StartGrabbing* and ends either when the requested number of images has
    been acquired or on a *StopGrabbing* command. If the number of images to
    grab is *-1* then the grab cycle does not stop automatically and has to be
    stopped by explicitly calling *stop_grab*. Internally the acquisition
    uses a circular array of image_buffers, when a buffer has been filled it is
    removed from the array until the user releases it. The default size of the
    buffer array is 10 can be changed with the ``MaxNumBuffer`` property,

    If the user does not process the images at the same rate as these are
    acquired the camera must apply a strategy:

    - **GrabStrategy_OneByOne**: Fill all the available buffers then wait
      until one becomes available.
    - **UGrabStrategy_UpcomingImage**: Serves always the next image
      (only uses 1 buffer).
    - **GrabStrategy_LatestImages**: Keep acquiring rewriting the buffers,
    - **GrabStrategy_LastImageOnly**: Serves always the last image acquired
      (uses only 1 buffer)

    If you need to acquire a sequence of images (usually started using a
    trigger) use *"GrabStrategy_OneByOne"*, if you want to read the last
    image and drop all images in-between use  *"GrabStrategy_LatestImageOnly"*.
    These are the two most common strategies.

    Args:
        camera: The user defined name of the camera, if None the first
            available camera is used.
        number_of_images: The number of images to grab, defaults to infinite
            (-1).
        grab_strategy: The strategy to use in handling missed frames.
        start: If True grabbing is started by the constructor, defaults to
            False
        verbose: Verbosity level, one of *LOG_INFO*, *LOG_WARNING*,
            *LOG_ERROR*, defaults to *LOG_WARNING*.
        opt_settings: Dictionary of parameter-name/value to send to the camera.
        """

    grab_strategies = {"GrabStrategy_LatestImageOnly":
                           pylon.GrabStrategy_LatestImageOnly,
                       "GrabStrategy_LatestImages":
                           pylon.GrabStrategy_LatestImages,
                       "GrabStrategy_UpcomingImage":
                           pylon.GrabStrategy_UpcomingImage,
                       "GrabStrategy_OneByOne": pylon.GrabStrategy_OneByOne}

    def __init__(self, camera: Optional[str] = None,
                 number_of_images: Optional[int] = -1,
                 grab_strategy: Optional[str] = "GrabStrategy_LatestImageOnly",
                 start: Optional[bool] = False,
                 verbose: Optional[int] = LOG_WARNING,
                 opt_settings: Optional[dict] = {}):
        self.camera_name = camera
        self.number_of_images = number_of_images
        self.verbose = verbose

        try:
            self.grab_strategy = self.grab_strategies[grab_strategy]
        except:
            self.grab_strategy = pylon.GrabStrategy_OneByOne

        self.open(camera)

        for k in opt_settings:
            try:
                self.camera.__getattr__(k).SetValue(opt_settings[k])
            except:
                self.log(LOG_ERROR,
                         "Error setting argument: {:s}, value: {:s}".
                         format(k, str(opt_settings[k])))

        if start:
            self.start()

    def log(self, level: int, *args):
        if level >= self.verbose:
            print(*args)

    def open(self, camera_name: Optional[str] = None) -> bool:
        """Try to open the camera with *user defined name* equal to
        *camera_name*

        Args:
            camera_name: The user defined name of the camera to open, defaults
                to the name declared at construction time, if even that is None
                the first available camera is used.

        Returns:
              True on success, False otherwise.
        """
        try:
            self.close()

            if camera_name is None:
                camera_name = self.camera_name

            factory = pylon.TlFactory.GetInstance()
            self.available_cameras = factory.EnumerateDevices()
            for i, cam in enumerate(self.available_cameras):
                cls = cam.GetDeviceClass()
                if cls == "BaslerUsb":
                    name = cam.GetFriendlyName()
                else:
                    name = cam.GetUserDefinedName()

                self.log(LOG_INFO, name, camera_name)
                if camera_name is None:
                    # If no camera was ever defined we use the first available
                    # camera
                    camera_name = name
                if name == camera_name:
                    self.camera_name = name
                    self.camera = pylon.InstantCamera(
                        factory.CreateDevice(self.available_cameras[i]))
                    self.log(LOG_INFO, "created")
                    self.camera.Open()
                    self.log(LOG_INFO, self.is_open())
                    self.log(LOG_INFO, "get_socket_buffer_size:",
                             self.get_socket_buffer_size())
                    return True
        except:
            print_exc()

        return False

    def get_camera_name(self) -> str:
        """Returns the name of the camera"""
        return self.camera_name

    def close(self):
        """Close the camera and free the resources"""
        try:
            self.camera.Close()
        except:
            pass

        self.camera = None

    def has_been_removed(self):
        """Returns True if the camera has been removed, False otherwise."""
        try:
            return self.camera.IsCameraDeviceRemoved()
        except:
            return True

    def is_open(self):
        """Returns True if the camera has been opened, False otherwise."""
        try:
            return self.camera.IsOpen()
        except:
            return False

    def start(self, number_of_images: Optional[int] = None,
              grab_strategy: Optional[str] = None):
        """Starts the grabbing sequencer. If no parameter is passed the
        last values passed to either start of init are used.

        Args:
            number_of_images: The number of images to grab.
            grab_strategy: The strategy to use in handling missed frames.
        """
        if number_of_images is not None:
            self.number_of_images = number_of_images
        if grab_strategy is not None:
            self.grab_strategy = grab_strategy

        try:
            if self.number_of_images > 0:
                self.camera.StartGrabbingMax(self.number_of_images,
                                             self.grab_strategy)
            else:
                self.camera.StartGrabbing(self.grab_strategy)
        except:
            # print_exc()
            pass

    def stop(self) -> bool:
        """Stops the grabbing sequence (if active). returns True on success,
        False otherwise (including if grabbing was not active)."""
        try:
            if self.camera.IsGrabbing():
                self.camera.StopGrabbing()
                return True
        except:
            # print_exc()
            pass

        return False

    def is_grabbing(self):
        """Returns True if grabbing is active, False otherwise."""
        try:
            return self.camera.IsGrabbing()
        except:
            pass

    def get_num_ready_buffers(self) -> int:
        """Returns the number of buffers ready to be read-out"""
        return self.camera.NumReadyBuffers.GetValue()

    def get_max_num_buffer(self) -> int:
        """Returns the size of the image-buffers array."""
        return self.camera.MaxNumBuffer.GetValue()

    def set_max_num_buffer(self, num):
        """Sets the size of the image-buffers array."""
        return self.camera.MaxNumBuffer.SetValue(num)

    def get_tick_frequency(self) -> float:
        """Returns the Frequency of the internal counter clock."""
        # TODO check if float and units
        return self.camera.GevTimestampTickFrequency.GetValue()

    def reset_time_stamps(self):
        """Resets the internal timestamp counter."""
        return self.camera.GevTimestampControlReset.Execute()

    def guess_property_name(self, prop):
        """Some camera add a suffix to certain properties like Exposure,
        other ExposreRaw or FrameRate -> FrameRateAbs. Derives from a
        change in the GENICAM specification across versions.

        Args:
            prop: the name of a property

        Returns:
            The actual name of the property or None if none is found.
        """
        for sfx in ["", "Raw", "Abs"]:
            try:
                _prop = prop + sfx
                self.camera.__getattr__(_prop)
                return _prop
            except:
                pass
        return None

    def validate_property(self, prop: str, val: Any) -> Any:
        """The proposed value for the property is validated against the
        information provided in the relative camera node.

        For numeric fields, in case the requested value is not compatible with
        the *increment* it will be *floor* to the closest accepted value.

        Args:
            prop: the name of the property.
            val: The proposed value.

        Returns:
            The closest value accepted by the property or None if nothing
            valid found.
        """
        try:
            range = self.get_property_range(prop)
            # print(range)
            if isinstance(range[0], str):
                if val not in range:
                    return None
                else:
                    return val
            else:
                try:
                    if val < range[0]:
                        return range[0]
                    elif val > range[1]:
                        return range[1]
                    # In case the value is not compatible with teh increment
                    # we set it to the lower accepted value
                    if range[2]:
                        return val - (val - range[0]) % range[2]
                    else:
                        return val
                except:
                    print_exc()
                    self.log(LOG_ERROR, "Offending range", prop, val, range)
                    return range[0]
        except:
            return None

    def get_property_range(self, prop: str) -> list:
        """Returns the valid range for *prop*. If the node is an enumeration
        the returned list contains the valid strings. If the node is bool or
        numeric the returned list contains the *min*, *max* , *increment*
        and *current* values"""
        res = None
        try:
            _prop = self.guess_property_name(prop)
            attr = self.camera.__getattr__(_prop)
            if isinstance(attr, genicam.IBoolean):
                res = [0, 1, 0, attr.GetValue()]
            elif isinstance(attr, genicam.IEnumeration):
                res = [i.Symbolic for i in attr.Entries]
            elif isinstance(attr, (genicam.IFloat, genicam.IInteger)):
                try:
                    increment = attr.GetInc()
                except:
                    increment = 0
                res = [attr.GetMin(), attr.GetMax(), increment,
                       attr.GetValue()]
        except:
            self.log(LOG_ERROR, "Property {:s} not available".format(prop))
            print_exc()

        return res

    def get_property(self, prop: str) -> Any:
        """Returns the value of *prop* or None if *prop* is not available.
        If *prop* is not found the *like* versions are tried as well."""
        try:
            _prop = self.guess_property_name(prop)
            return self.camera.__getattr__(_prop).GetValue()
        except:
            return None

    def get_properties(self, prop: str,
                       with_range: Optional[bool] = False) -> dict:
        """Returns a dictionary of property/values.

        Args:
            prop: a list of properties to read.
            with_range: If True an entry *propertyname_range* is included in
                the dictionary with the return of ``get_property_range``.
        """
        res = {}
        for k in prop:
            try:
                res[k] = self.get_property(k)
                if with_range:
                    res[k + "_range"] = self.get_property_range(k)
            except:
                pass

        return res

    def set_property(self, prop: str, val: Any,
                     validate: Optional[bool] = True) -> Any:
        """Sets the value of the property *prop*. If *validate* is True
        (default) the value is compared with the ``get_property_range``
        result. If *prop* is not found the *like* versions are tried as well.

        Returns:
            The value set (can be different from the requested value if
            *validate* is  True). *None* is returned in case of failure.
        """

        self.log(LOG_INFO, f"set_property {prop} {val}")
        try:
            _prop = self.guess_property_name(prop)
            if validate:
                val = self.validate_property(_prop, val)
                if val is None:
                    self.log(LOG_WARNING, f"Set {prop}:{val} Not valid")
                    return None
            # print(val)
            self.camera.__getattr__(_prop).SetValue(val)
            return val
        except:
            print_exc()
            return None

    def set_properties(self, properties: dict, validate=True):
        """For every item in *properties* call
        ``set_property(key, value, validate)``
        """
        for k in properties:
            try:
                self.set_property(k, properties[k], validate)
            except:
                pass

    def get_gain(self) -> int:
        """Returns the value of *Gain*"""
        try:
            return self.get_property("Gain")
        except:
            return None

    def set_gain(self, gain: int, validate=True):
        """Sets the value of *Gain*"""
        try:
            self.set_property("Gain", gain, validate)
        except:
            pass

    def get_black_level(self) -> int:
        """Returns the value of *BlackLevel*"""
        try:
            return self.get_property("BlackLevel")
        except:
            return None

    def set_black_level(self, level: int, validate=True):
        """Sets the value of *BlackLevel*"""
        try:
            self.set_property("BlackLevel", level, validate)
        except:
            pass


    def get_exposure(self) -> int:
        """Returns the value of *ExposureTime* [us]"""
        try:
            return self.get_property("ExposureTime")
        except:
            return None

    def set_exposure(self, exposure: int, validate=True):
        """Sets the value of *ExposureTime* [us]"""
        try:
            self.set_property('ExposureAuto', 'Off', validate=validate)
            self.set_property("ExposureTime", exposure, validate=validate)
        except:
            pass

    def get_frame_rate(self) -> tuple:
        """Returns a tuple (max_frame_rate, rate_limit_enabled)"""
        try:
            return (self.get_property("AcquisitionFrameRate"),
                    self.get_property("AcquisitionFrameRateEnable"))
        except:
            return None

    def set_frame_rate(self, rate: float, enable: bool, validate=True):
        """Sets the maximum frame rate and the enable_flag."""
        try:
            self.set_property("AcquisitionFrameRateEnable", enable,
                              validate=validate)
            self.set_property("AcquisitionFrameRate", rate, validate=validate)
        except:
            pass

    def get_binning(self) -> tuple:
        """Returns a tuple with the horizontal and vertical binning factor and
        binning modes (enum as str).

        (BinningHorizontal, BinningVertical, BinningHorizontalMode,
         BinningVerticalMode)."""
        try:
            return (self.get_property("BinningHorizontal"),
                    self.get_property("BinningVertical"),
                    self.get_property("BinningHorizontalMode "),
                    self.get_property("BinningVerticalMode  "))
        except:
            return None

    def set_binning(self, binning_x: int, binning_y: int,
                    binning_x_mode: Optional[str] = 'Sum',
                    binning_y_mode: Optional[str] = 'Sum',
                    validate: Optional[bool] = True):
        """Sets the value of horizontal and vertical binning factors and modes.

        Factors default to 1 and modes default to Sum.

        Valid values are:
            - Factors  [1, 2, 3, 4]
            - Modes ['Sum', 'Average'].
        """
        try:
            self.set_property("BinningHorizontal", binning_x,
                              validate=validate)
            self.set_property("BinningVertical", binning_y, validate=validate)
        except:
            pass

    def get_AOI(self) -> tuple:
        """Returns a tuple with the Area Of Interest, i.e. the image rectangle
        relative to the whole sensor area that is actually acquired. If binning
        is active the sensor area is the rescaled sensor area.

        Offsets are calculated from the top-left corner.

        Returns: (OffsetX, OffsetY, Width, Height)
        """
        try:
            return (self.get_property("OffsetX"), self.get_property("OffsetY"),
                    self.get_property("Width"), self.get_property("Height"))
        except:
            pass

    def set_AOI(self, x: int, y: int, width: Optional[int] = -1,
                height: Optional[int] = -1,
                validate: Optional[bool] = True):
        """Sets the Area of Interest.

        Args:
            x: Left offset.
            y: Top offset.
            width: The width of the rectangle, if negative the AOI will be as
                large as possible. defaults to -1.
            height: The height of the rectangle, if negative the AOI will be
                as tall as possible. defaults to -1.
        """
        try:
            # Set offsets to 0 so width and height are accepted (if < max)
            self.set_property("OffsetX", 0, validate=validate)
            self.set_property("OffsetY", 0, validate=validate)
            if width < 0:
                width = self.get_property("maxWidth") - x
            if height < 0:
                height = self.get_property("maxHeight") - y
            self.set_property("Width", width, validate=validate)
            self.set_property("Height", height, validate=validate)
            self.set_property("OffsetX", x, validate=validate)
            self.set_property("OffsetY", y, validate=validate)
        except:
            pass

    def get_packet_config(self) -> tuple:
        """Returns a tuple with the intra packet transmission delay in ticks
        (8ns) and max packet size in bytes (GevSCPD, GevSCPSPacketSize)."""
        try:
            return (self.get_property("GevSCPD"),
                    self.get_property("GevSCPSPacketSize"))
        except:
            pass

    def set_packet_config(self, delay: Optional[int] = 0,
                          size: Optional[int] = 1500,
                          validate: Optional[bool] = True):
        """Sets the intra packet transmission delay in ticks
        (8ns) and max packet size in bytes. Default values are
        0 and 1500 respectively."""
        try:
            self.set_property("GevSCPD", delay, validate=validate)
            self.set_property("GevSCPSPacketSize", size, validate=validate)
        except:
            pass

    def get_socket_buffer_size(self) -> int:
        """Returns the socket buffer size in kbytes."""
        try:
            return self.camera.StreamGrabber.SocketBufferSize.GetValue()
        except:
            pass

    def set_socket_buffer_size(self, size: int):
        """Sets the socket buffer size in kbytes."""
        try:
            self.camera.StreamGrabber.SocketBufferSize.GetValue(size)
        except:
            pass

    def get_pixel_format(self) -> str:
        """Returns the pixel format (enum as str)"""
        try:
            return self.get_property("PixelFormat")
        except:
            return None

    def set_pixel_format(self, format: Optional[str] = "Mono8",
                         validate: Optional[bool] = True):
        """Sets the pixel format, defaults to 'Mono8'. The available values
        depend on the camera model."""
        try:
            self.set_property("PixelFormat", format, validate=validate)
        except:
            pass

    def get_trigger_delay(self) -> int:
        """Returns the trigger delay in us."""
        try:
            return self.get_property("TriggerDelay")
        except:
            return None

    def set_trigger_delay(self, delay: int, validate: Optional[bool] = True):
        """Sets the trigger delay in us. [0-1 second]"""
        try:
            self.set_property("TriggerDelay", delay, validate=validate)
        except:
            pass

    def get_trigger_edge(self) -> str:
        """Returns the trigger active edge (enum as str)."""
        try:
            return self.get_property("TriggerActivation")
        except:
            return None

    def set_trigger_edge(self, edge: Optional[str] = "RisingEdge",
                         validate: Optional[bool] = True):
        """Sets the trigger edge, defaults to 'RisingEdge'.

        Possible values:
            - **RisingEdge**: The trigger becomes active when the trigger
              signal rises, i.e., when the signal status changes from low to
              high.
            - **FallingEdge**: The trigger becomes active when the trigger
              signal falls, i.e., when the signal status changes from high to
              low.
            - **AnyEdge** (if available): The trigger becomes active when the
              trigger signal falls or rises.
            - **LevelHigh** (if available): The trigger is active as long as
              the trigger signal is high.
            - **LevelLow** (if available): The trigger is active as long as the
              trigger signal is low."""
        try:
            self.set_property("TriggerActivation", edge, validate=validate)
        except:
            pass

    def get_trigger_mode(self):
        """Returns a tuple with the trigger ON/OFF status and the trigger
        source."""
        try:
            return (self.get_property("TriggerMode"),
                    self.get_property("TriggerSelector"))
        except:
            return None

    def set_trigger_mode(self, mode: Optional[str] = "Off",
                         selector: Optional[str] = "FrameStart",
                         validate: Optional[bool] = True):
        """Sets the trigger ON/OFF status and the trigger source.

        In FrameStart mode the trigger starts the exposure of each image.

        In AcquisitionStart mode the trigger starts the exposure of the first
        frame, then all successive frames of the sequence are acquired
        asynchronously.

        Other selectors may be available on different camera models.

        Args:
            mode: 'On' or 'Off', defaults to 'Off'.
            selector: One of ['FrameStart', 'AcquisitionStart'], defaults to
                FrameStart
            """

        try:
            self.set_property("TriggerMode", mode, validate=validate)
            self.set_property("TriggerSelector", selector, validate=validate)
        except:
            pass

    def get_trigger_source(self) -> str:
        """Returns the trigger source (enum as str)."""
        try:
            return self.get_property("TriggerSource")
        except:
            return None

    def set_trigger_source(self, source: Optional[str] = "Software",
                           validate: Optional[bool] = True):
        """Sets the trigger source.

        Some sources are common to all cameras like 'Software', most
        cameras also provide HW triggers like 'Line1', 'Line2' etc.

        default source is 'Software'
        """
        try:
            self.set_property("TriggerSource", source, validate=validate)
        except:
            pass

    def send_software_trigger(self):
        """Perform a software trigger."""
        self.camera.ExecuteSoftwareTrigger()

    def get_test_image(self) -> str:
        """Returns the selected test image (enum as str)"""
        try:
            return self.get_property("TestImageSelector")
        except:
            return None

    def set_test_image(self, test_image: str, validate: Optional[bool] = True):
        """Sets the test image.

        Possible values are: ['Testimage1', 'Testimage2', 'Testimage3',
            'Testimage4', 'Testimage5', \*'Testimage6']"""
        try:
            self.set_property("TestImageSelector", test_image,
                              validate=validate)
        except:
            pass

    def get_next_image(self, time_out=1000):
        """retrieve the next ready image buffer. To wWhich image it corresponds
        depends on the grab strategy. By default after 1 second the call will
        timeout, but it can be changed by defining a different *time_out* value
        in [ms].

        Returns:
            A tuple with the internal counter-timestamp of the image and the
            image data buffer itself as ImageData (subclass of numpy ndarray).
        """
        try:
            grabResult = self.camera.RetrieveResult(time_out,
                                                    pylon.TimeoutHandling_Return)
            if grabResult.IsValid():
                data = ImageData(grabResult.Array, dtype=np.int16,
                                t_stamp=grabResult.GetTimeStamp(),
                                pixel_format=grabResult.GetPixelType())
                return (data.t_stamp, data)
        except:
            self.log(LOG_ERROR, "get_next_image")
            self.log(LOG_ERROR, sys.exc_info())

        return (None, None)
