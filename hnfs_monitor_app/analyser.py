import numpy as np

from scipy.constants import e, h, c, pi

class Analyser:
    def __init__(self,
                 pixel_size= 3.45e-6, M=21.4, NA=0.4,
                 E_ph= 12.377, 
                 wavelength_YAG= 632.8e-9, t_sample = 2e-3):    
        self.pixel_size = pixel_size
        self.M = M
        self.eff_pixel_size = pixel_size / M
        self.NA = NA
        self.wavelength = h * c / (E_ph * 1e3 * e)
        self.k = 2 * pi / self.wavelength
        self.wavelength_YAG = wavelength_YAG
        self.t_sample = t_sample      
        
        self.q_min = .1e6   # minimum meaningful q
  
    def update_optics(self, M, pixel_size):
        self.pixel_size = pixel_size
        self.M = M
        self.eff_pixel_size = pixel_size / M
        
    def compute_ps(self, data, remove_DC=True):
        ps = np.fft.fftshift((np.abs(np.fft.fft2(data)))**2)
        ny, nx = np.shape(ps)
        q_bin_x = 2 * pi / (nx * self.eff_pixel_size)
        q_bin_y = 2 * pi / (ny * self.eff_pixel_size)
        xax = np.linspace(-nx * q_bin_x / 2, nx * q_bin_x / 2, nx)
        yax = np.linspace(-ny * q_bin_y / 2, ny * q_bin_y / 2, ny)
        ps_dict = {"xax": xax, "yax": yax, "data": ps}
        
        if remove_DC:
            ps_dict = self.remove_DC(ps_dict)
            
        return ps_dict
    
    def remove_DC(self, ps_dict):
        
        xx, yy = np.meshgrid(ps_dict["xax"], ps_dict["yax"])
        rr = np.sqrt(xx**2 + yy**2)
        
        ps_dict["data"] = np.where(rr<=self.q_min, np.nan, ps_dict["data"])
        
        return ps_dict
    
    def sector_average(self, ps_dict, theta_centre, theta_width):   
        
        data = ps_dict["data"]
        
        from_theta = theta_centre - theta_width / 2
        to_theta = theta_centre + theta_width / 2
        
        xx, yy = np.meshgrid(np.linspace(-(np.shape(data)[1]-1)/2, \
                                          (np.shape(data)[1]-1)/2, \
                                           np.shape(data)[1]),     \
                             np.linspace(-(np.shape(data)[0]-1)/2, \
                                          (np.shape(data)[0]-1)/2, \
                                           np.shape(data)[0]))
        theta = np.arctan2(yy, xx)
        rr = np.sqrt(xx**2 + yy**2)
        dr = np.diff(ps_dict["xax"])[0]    # t.b.m. for asymmetric mesh
        
        r_sel, val_sel = [], []
        sector_mask = np.where((theta>=np.deg2rad(from_theta)) & \
                               (theta<=np.deg2rad(to_theta)))
        masked_data = np.nan * np.ones(np.shape(data))
        
        for i in range(len(sector_mask[0])):
            r_sel.append(rr[sector_mask[0][i], sector_mask[1][i]])
            val_sel.append(data[sector_mask[0][i], sector_mask[1][i]])
            masked_data[sector_mask[0][i], sector_mask[1][i]] = \
                                data[sector_mask[0][i], sector_mask[1][i]]
        val_sel = np.array(val_sel)       
        bin_extremes = np.arange(0, 1 + np.ceil(np.sqrt(\
                                            ((np.shape(data)[0])/2)**2 + \
                                            ((np.shape(data)[1])/2)**2)), 1)    
        bin_centres = 0.5 + bin_extremes[:-1]
        idxs = np.digitize(r_sel, bin_extremes) - 1
        axis = np.array(bin_centres) * dr
        
        prof = []
        for b in range(len(bin_centres)):
            prof.append(np.nanmean(val_sel[idxs==b]))
        prof = np.array(prof)
        
        prof_dict = {"ax": axis, "data": prof}
        
        return prof_dict